#include "nativeCPUInfo.h"

#include <sys/syscall.h>
JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_NativeCPUInfo_getCpuId(JNIEnv *env, jobject thiz) {
    unsigned cpu;
    if (syscall(__NR_getcpu, &cpu, NULL, NULL) < 0) {
        return -1;
    } else {
        return (int) cpu;
    }
}
