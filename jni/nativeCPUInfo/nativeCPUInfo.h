#include <jni.h>

#ifndef __NATIVECPUINFO_H__
#define __NATIVECPUINFO_H__

#ifdef __cplusplus 
extern "C" { 
#endif
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_NativeCPUInfo_getCpuId(JNIEnv *env, jobject thiz);
#ifdef __cplusplus 
} 
#endif

#endif
