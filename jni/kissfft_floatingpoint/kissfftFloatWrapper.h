#include <jni.h>
#include "kiss_fastfir.h"

#ifndef __KISSFFT_WRAPPER_H__
#define __KISSFFT_WRAPPER_H__

//#define NULL 0

#ifdef __cplusplus 
extern "C" { 
#endif
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_FirFilter_getCpuId(JNIEnv *env, jobject thiz);
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_FirFilter_kissfftCreate(JNIEnv *env, jobject thiz,
			jobjectArray hrirsL, jobjectArray hrirsR, jint fftSize,	jint crossfadeOverlapSize, jint fftPaddingSize);
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_FirFilter_kissfftDestroy(JNIEnv *env, jobject thiz);
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_FirFilter_beginRenderingBlock(JNIEnv *env, jobject thiz);
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_FirFilter_endRenderingBlock(JNIEnv *env, jobject thiz, jboolean crossfade, jbyteArray out);
	JNIEXPORT jint JNICALL Java_net_ptrbrtz_adbs_FirFilter_kissfftFilter(JNIEnv *env, jobject thiz, jbyteArray in,
			jint hrtfIndex, jfloat sampleScaling);
	JNIEXPORT jlong JNICALL Java_net_ptrbrtz_adbs_FirFilter_jniNanoTime(JNIEnv *env, jobject thiz);
#ifdef __cplusplus 
} 
#endif

#endif
