#include <jni.h>

#ifndef __SUPERPOWEREDAUDIOIO_H__
#define __SUPERPOWEREDAUDIOIO_H__

#ifdef __cplusplus 
extern "C" { 
#endif
JNIEXPORT jboolean JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_setupSuperpowered(JNIEnv *env, jobject thiz,
		jint _internalBufSizeInFrames, jint _resampledInternalBufSizeInFrames, jint _outputBufSizeInFrames,
		jint internalSamplerate, jint outputSamplerate, jint _ringBufSizeMultiplier);
JNIEXPORT void JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_shutdownSuperpowered(JNIEnv *env, jobject thiz);
JNIEXPORT int JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_resample(JNIEnv *env, jobject thiz, jbyteArray input,
		jbyteArray output);
JNIEXPORT void JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_nativePlay(JNIEnv *env, jobject thiz, jboolean clearRingBuf);
JNIEXPORT void JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_nativePause(JNIEnv *env, jobject thiz);
JNIEXPORT void JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_nativeFlushAndPause(JNIEnv *env, jobject thiz);
JNIEXPORT void JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_linearFade(JNIEnv *env, jobject thiz, jbyteArray buf,
		jint numFrames, jint sign);
JNIEXPORT void JNICALL Java_net_ptrbrtz_adbs_SuperpoweredAudioIO_enqueueSamples(JNIEnv *env, jobject thiz,
		jbyteArray jBuf, jint numFrames);

#ifdef __cplusplus 
} 
#endif

#endif
