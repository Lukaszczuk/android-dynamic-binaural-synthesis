package net.ptrbrtz.adbs;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.SeekBar;
import net.ptrbrtz.adbs.android.utils.MathUtils;

public class ExponentialSeekBar extends SeekBar {
	private float expBase = 6;	// more is more
	private float expMin = 0.0f;
	private float expMax = 1.0f;
	private int decimalPlaces = -1; // default no rounding

	public ExponentialSeekBar(Context context) {
		super(context);
	}

	public ExponentialSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ExponentialSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public void setExpBase(float base) {
		expBase = base;
	}
	
	public float getExpBase() {
		return expBase;
	}
	
	public float getExpMin() {
		return expMin;
	}
	
	public void setExpMin(float expMin) {
		this.expMin = expMin;
	}
	
	public float getExpMax() {
		return expMax;
	}
	
	public void setExpMax(float expMax) {
		this.expMax = expMax;
	}
	
	public void setExpValue(float val) {
		setProgress(Math.round(MathUtils.mapExpToLin(val, expBase, 0, getMax(), getExpMin(), getExpMax())));
	}
	
	public float getExpValue() {
		float val = MathUtils.mapLinToExp((float) getProgress(), expBase, 0, getMax(), getExpMin(), getExpMax());
		if (decimalPlaces >= 0)
			val = MathUtils.roundTo(val, decimalPlaces);
		return val;
	}

	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(int decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}
}
