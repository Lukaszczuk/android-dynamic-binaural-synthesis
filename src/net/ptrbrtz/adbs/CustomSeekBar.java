package net.ptrbrtz.adbs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.SeekBar;
import net.ptrbrtz.adbs.R;

public class CustomSeekBar extends SeekBar {
	private static Paint paint;
	
	private int visualProgress; 

	public CustomSeekBar(Context context) {
		super(context);
		
		if (paint == null) {
			paint = new Paint();
			paint.setColor(Color.WHITE);
		}
		
		visualProgress = getProgress();
	}

	public CustomSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (paint == null) {
			paint = new Paint();
			paint.setColor(Color.WHITE);
		}
		
		visualProgress = getProgress();
	}

	public CustomSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		if (paint == null) {
			paint = new Paint();
			paint.setColor(Color.WHITE);
		}
		
		visualProgress = getProgress();
	}

	public void setVisualProgress(int progress) {
		visualProgress = progress;
	}
	
	public int getVisualProgress() {
		return visualProgress;
	}
	
	@Override
	protected synchronized void onDraw(Canvas canvas) {
		int width, height;
		width = this.getWidth();
		height = this.getHeight();

		canvas.drawColor(getResources().getColor(R.drawable.col_btn_default));
		
		canvas.drawLine(0, 0, width, 0, paint);
		canvas.drawRect(0, 0, Math.round((float) this.visualProgress / (float) this.getMax() * (float) width), height, paint);
	}
}
