package net.ptrbrtz.adbs;

import android.util.Log;

public class NativeCPUInfo {
	private static final String TAG = "NativeCPUInfo";

	// load shared lib
	static {
		try {
			System.loadLibrary("nativeCPUInfo");
			Log.d(TAG, "library loaded...");
		} catch (Exception e) {
			Log.e(TAG, "could not load library...");
		}
	}
	
	// native methods
	public static native int getCpuId(); // get cpu number this thread is running on
}
