package net.ptrbrtz.adbs.android.utils;

public class MathUtils {
	public static boolean isPowerOfTwo(int x) {
		return x != 0 && (x & (x - 1)) == 0;
	}
	
	// not the fastest way to do it, but good enough
	public static int getNextPowerOfTwo(int i) {
		int result = 1;
		while (result < i) result <<= 1;
		return result;
	}
	
	public static int getNextPowerOfTwo(int i, int base) {
		int result = base;
		while (result < i) result <<= 1;
		return result;
	}

	public static float roundTo(float val, int decimalPlaces) {
		float m = (float) Math.pow(10.0f, (float) decimalPlaces);
		return (float) Math.round(val * m) / m;
	}
	
	public static float mapLinToExp(float linVal, float expBase, float linMin, float linMax, float expMin, float expMax) {
		linVal = (linVal - linMin) / (linMax - linMin); // normalize

		float expVal = (float) (Math.pow(expBase, linVal) - 1.0f) / (expBase - 1.0f);
		return expVal * (expMax - expMin) + expMin; // de-normalize
	}

	public static float mapExpToLin(float expVal, float expBase, float linMin, float linMax, float expMin, float expMax) {
		expVal = (expVal - expMin) / (expMax - expMin); // normalize

		float linVal = (float) (Math.log(expVal * (expBase - 1.0f) + 1.0f) / Math.log(expBase));
		return linVal * (linMax - linMin) + linMin; // de-normalize
	}
}
