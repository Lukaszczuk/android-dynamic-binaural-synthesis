package net.ptrbrtz.adbs;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;

public class Listener extends Entity {
	private static final String TAG = "Reference";
	
	// static fields for drawing
	protected static Paint paint = null;
	protected static Picture arrowPicture = null;
	
	public static Paint getPaint() {
		return paint;
	}

	public static void setPaint(Paint paint) {
		Listener.paint = paint;
	}

	@Override
	public void draw(Canvas canvas, float inverseScaling, float rotation) {
		// save current transformation matrix
		canvas.save();
		
		// rotate and scale
		canvas.rotate(azimuth);
		canvas.scale(inverseScaling, inverseScaling);
		
		// draw reference
		arrowPicture.draw(canvas);
		
		// restore matrix
		canvas.restore();
	}
}
