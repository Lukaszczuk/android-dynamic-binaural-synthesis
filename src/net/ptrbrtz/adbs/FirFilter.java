package net.ptrbrtz.adbs;

import android.util.Log;

public class FirFilter {
	private static final String TAG = "FirFilter";

	// load shared lib
	static {
		try {
			System.loadLibrary("firfilter");
			Log.d(TAG, "library loaded...");
		} catch (Exception e) {
			Log.e(TAG, "could not load library...");
		}
	}
	
	// native methods
	public static native int kissfftCreate(float[][] hrirsL, float[][] hrirsR, int fftSize, int crossfadeSize, int paddingSize);
	public static native int kissfftDestroy();
	public static native int beginRenderingBlock();
	public static native int endRenderingBlock(boolean crossfade, byte[] audioOutput);
	public static native int kissfftFilter(byte[] audioInput, int hrtfIndex, float sampleScaling);
	public static native long jniNanoTime();
}
